package com.example.owner.finappprobno.models;

/**
 * Created by owner on 10.10.2017 г..
 */

public class Expense {
    private String expense;
    private double expenseValue;

    public Expense(){

    }

    public Expense(String expense, double expenseValue){
        this.expense = expense;
        this.expenseValue = expenseValue;
    }

    public void setExpense(String expense){
        this.expense = expense;
    }
    public String getExpense(){
        return  expense;
    }
    public void setExpenseValue(double expenseValue){
        this.expenseValue = expenseValue;
    }
    public double getExpenseValue(){
        return expenseValue;
    }
}
