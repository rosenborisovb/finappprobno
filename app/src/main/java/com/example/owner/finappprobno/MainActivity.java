package com.example.owner.finappprobno;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.example.owner.finappprobno.sample.SampleDataProvider;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

//    JSONArray jsonArray = SampleDataProvider.jsonArray;
//    List<String> expense = new ArrayList<>();
    private TextView mLog;
//    List<Expense> expense = SampleDataProvider.expense;
    JSONArray mJsonArray = SampleDataProvider.jsonArray;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ListView listView = (ListView) findViewById(R.id.list_costs);

        ArrayList<String> lstExpense = new ArrayList<String>();
        if (mJsonArray != null){
            for (int i = 0; i < mJsonArray.length(); i ++){
                try {
                    lstExpense.add(mJsonArray.get(i).toString());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
        ArrayAdapter adapter = new ArrayAdapter(this, android.R.layout.simple_list_item_1, lstExpense);
        listView.setAdapter(adapter);

//        mLog = (TextView) findViewById(R.id.tvLog);
//        mLog.setText("");
//
//        for (Expense expense1 : expense ){
//            mLog.append(expense1.getExpense() + "\n");
//        }

//        try {
//            for (int i = 0; i < jsonArray.length(); i++) {
//                JSONObject item = (JSONObject) jsonArray.get(i);
//                String itemName = item.getString("expense");
//                double itemPrice = item.getDouble("expenseValue");
////                ArrayAdapter adapter = new ArrayAdapter(this, android.R.layout.simple_list_item_1, expense);
//                StringBuilder builder = new StringBuilder(itemName)
//                        .append(" ")
//                        .append(itemPrice + "\n");
////                listView.setAdapter(adapter);
//                mLog.append(builder.toString());
//            }
//        }catch(JSONException e) {
//            e.printStackTrace();
//        }


    }
}
