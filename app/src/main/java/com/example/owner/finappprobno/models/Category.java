package com.example.owner.finappprobno.models;

/**
 * Created by owner on 10.10.2017 г..
 */

public class Category {
    private String categoryName;

    public Category(){

    }

    public Category(String categoryName){
        this.categoryName = categoryName;
    }

    public void setCategoryName(String categoryName){
        this.categoryName = categoryName;
    }
    public String getCategoryName(){
        return categoryName;
    }
}
